package transiant

type Api struct {
	KeyGenerate func() (*KeyPair, error)
}

func makeBaseApi() *Api {
	return &Api{
		KeyGenerate: apiKeyGenerate,
	}
}

func apiKeyGenerate() (*KeyPair, error) {
	return makeKeyPair()
}
