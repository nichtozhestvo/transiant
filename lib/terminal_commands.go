package transiant

import (
	"bytes"
	"encoding/hex"
	"os"

	"github.com/c-bata/go-prompt"
	"github.com/spf13/cobra"
)

var rootCmd = &cobra.Command{
	Long: "Transiant Client",
	Run: func(cmd *cobra.Command, args []string) {
		cmd.Help()
	},
}

var exitCmd = &cobra.Command{
	Use:   "exit",
	Short: "exit program",
	Run: func(cmd *cobra.Command, args []string) {
		os.Exit(0)
	},
}
var keypairCmd = &cobra.Command{
	Use:   "keypair",
	Short: "show keypair",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		kpi, err := TERMINAL_CTX.vault.Get("keypair")
		if err != nil {
			cmd.PrintErr(err)
			return
		} else {
			kp := (*kpi).(KeyPair)
			cmd.Printf("private: %s, public: %s, created_at: %s",
				hex.EncodeToString(kp.Private[:]),
				hex.EncodeToString(kp.Public[:]),
				kp.CreatedAt)
		}

	},
}

var keypairCmd_New = &cobra.Command{
	Use:   "new",
	Short: "Generate key pair",
	Run: func(cmd *cobra.Command, args []string) {
		kp, err := makeKeyPair()
		if err != nil {
			cmd.PrintErr(err)
			return
		}
		err = TERMINAL_CTX.vault.Set("keypair", kp)
		if err != nil {
			cmd.PrintErr(err)
			return
		}
		cmd.Print("Key pair generated")
	},
}

var serviceCmd = &cobra.Command{
	Use: "service",
	Run: func(cmd *cobra.Command, args []string) {
		cmd.Println(cmd.UseLine())
	},
}

var serviceCmd_Start = &cobra.Command{
	Use: "start",
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) == 0 {
			cmd.PrintErr("Missing service name")
			return
		}
		err := SERVICE_MANAGER.Start(args[0])
		if err != nil {
			cmd.PrintErr(err)
		}
	},
}

var serviceCmd_Stop = &cobra.Command{
	Use: "stop",
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) == 0 {
			cmd.PrintErr("Missing service name")
			return
		}
		err := SERVICE_MANAGER.Stop(args[0])
		if err != nil {
			cmd.PrintErr(err)
		}
	},
}

func init() {
	serviceCmd.AddCommand(serviceCmd_Start)
	serviceCmd.AddCommand(serviceCmd_Stop)

	keypairCmd.AddCommand(keypairCmd_New)

	rootCmd.AddCommand(serviceCmd)
	rootCmd.AddCommand(keypairCmd)
	rootCmd.AddCommand(exitCmd)
}

type OutStdErr struct {
	errBuffer *bytes.Buffer
	outBuffer *bytes.Buffer
}

func (oe *OutStdErr) Init() {
	oe.errBuffer = bytes.NewBufferString("")
	oe.outBuffer = bytes.NewBufferString("")
}

func (oe *OutStdErr) Out() string {
	return oe.outBuffer.String()
}

func (oe *OutStdErr) Err() string {
	return oe.errBuffer.String()
}

func makeOutStdErr() *OutStdErr {
	se := OutStdErr{}
	se.Init()
	return &se
}

func ExecuteCmd(ctx interface{}, args []string) (*OutStdErr, error) {
	se := makeOutStdErr()
	rootCmd.SetOut(se.outBuffer)
	rootCmd.SetErr(se.errBuffer)
	rootCmd.SetArgs(args)
	if err := rootCmd.Execute(); err != nil {
		return nil, err
	}
	return se, nil
}

func MakeSuggestions(cmd *cobra.Command) []prompt.Suggest {
	ps := []prompt.Suggest{}
	for _, suggest := range cmd.SuggestionsFor(cmd.Name()) {
		ps = append(ps, prompt.Suggest{Text: suggest})
	}
	return ps
}

func GetRootCommand() *cobra.Command {
	return rootCmd
}
