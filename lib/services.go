package transiant

import (
	"fmt"
	"log"
)

type TStatus int

func (s TStatus) String() string {
	return ""
}

const (
	ERROR TStatus = iota
	STOPPED
	STARTED
)

type Service interface {
	Init()
	Name() string
	Start() error
	Stop() error
	Status() TStatus
	IsStatus(TStatus) bool
	Depends() []string
	IsInitialized() bool
}

type ServiceManager struct {
	services map[string]Service
}

var SERVICE_MANAGER *ServiceManager

func (s *ServiceManager) Init() *ServiceManager {
	s.services = map[string]Service{}
	return s
}

func (s *ServiceManager) Add(isrv *Service) error {
	service := *isrv
	name := service.Name()
	_, ok := s.Get(name)
	if ok {
		panic(fmt.Errorf("AddServiceError<%s>", name))
	}
	// service.Init()
	s.services[name] = service
	return nil
}

func (s *ServiceManager) Get(name string) (Service, bool) {
	service, ok := s.services[name]
	if !ok {
		return nil, ok
	}
	return service, true
}

func (s *ServiceManager) Start(name string) error {
	service, ok := s.Get(name)
	if !ok {
		return fmt.Errorf("ServiceStartError<%s>", name)
	}
	if service.IsStatus(STARTED) {
		return fmt.Errorf("ServiceNotStopped<%s>", name)
	}
	for _, depend := range service.Depends() {
		s.Start(depend)
	}
	if !service.IsInitialized() {
		service.Init()
	}
	service.Start()
	return nil
}

func (s *ServiceManager) Stop(name string) error {
	service, ok := s.Get(name)
	if !ok {
		return fmt.Errorf("ServiceStopError<%s>", name)
	}
	if !service.IsStatus(STARTED) {
		return fmt.Errorf("ServiceNotStarted<%s>", name)
	}
	return service.Stop()
}

func (s *ServiceManager) Status(name string) TStatus {
	service, ok := s.Get(name)
	if !ok {
		return ERROR
	}
	return service.Status()
}

func makeServiceManager() *ServiceManager {
	return &ServiceManager{
		services: map[string]Service{},
	}
}

/*
	BonjourService
*/

type BonjourService struct {
	status TStatus
}

func (bj BonjourService) Init() {
	bj.status = STOPPED
}

func (bj BonjourService) Depends() []string {
	return []string{"core"}
}

func (bj BonjourService) Name() string {
	return "bonjour"
}

func (bj BonjourService) Start() error {
	bj.status = STARTED
	return nil
}

func (bj BonjourService) Stop() error {
	log.Print("Stop bonjour")
	bj.status = STOPPED
	return nil
}

func (bj BonjourService) Status() TStatus {
	return bj.status
}

func (bj BonjourService) IsStatus(status TStatus) bool {
	if bj.status == status {
		return true
	}
	return false
}

func (bj BonjourService) IsInitialized() bool {
	return true
}

/*
	CoreService
*/

type CoreService struct {
	status TStatus
	core   *Core
}

func (cs CoreService) Init() {
	if cs.core != nil {
		return
	}
	cs.status = STOPPED
	cs.core = MakeCore()
	log.Printf("Service init core %v", cs.core)
}

func (cs CoreService) Depends() []string {
	return []string{}
}

func (cs CoreService) Name() string {
	return "core"
}

func (cs CoreService) Status() TStatus {
	return cs.status
}

func (cs CoreService) Start() error {
	go cs.core.Start()
	cs.status = STARTED
	return nil
}

func (cs CoreService) Stop() error {
	cs.core.SendMessage(CoreMessage{command: "exit"}, CHAN_CORE)
	cs.status = STOPPED
	return nil
}

func (cs CoreService) IsStatus(status TStatus) bool {
	if cs.status == status {
		return true
	}
	return false
}

func (cs CoreService) IsInitialized() bool {
	return cs.core != nil
}

/*
	MockService
*/

type MockService struct {
	status TStatus
}

func (cs MockService) Init() {
	cs.status = STOPPED
	log.Printf("%s service initialized", cs.Name())
}

func (cs MockService) Depends() []string {
	return []string{}
}

func (cs MockService) Name() string {
	return "mock"
}

func (cs MockService) Status() TStatus {
	return cs.status
}

func (cs MockService) Start() error {
	cs.status = STARTED
	log.Printf("%s service started", cs.Name())
	return nil
}

func (cs MockService) Stop() error {
	cs.status = STOPPED
	return nil
}

func (cs MockService) IsStatus(status TStatus) bool {
	if cs.status == status {
		return true
	}
	return false
}

func (cs MockService) String() string {
	return fmt.Sprintf("[%s] status: %s", cs.Name(), cs.status.String())
}

func (bj MockService) IsInitialized() bool {
	return true
}
