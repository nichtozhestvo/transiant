package transiant

import (
	"fmt"
	"log"
	"os"
	"strings"
	"time"

	"github.com/oleksandr/bonjour"
)

type BonjourSymbols struct {
	Instance string
	Service  string
	Domain   string
	NodeAddr string
}

var bonjourSymbols = BonjourSymbols{
	Instance: "Transiant Advertising",
	Service:  "_transiant._tcp",
	Domain:   "local.",
	NodeAddr: "node.addr",
}

type BonjourAdvertising struct {
	NodeAddr string
}

type BonjourConfig struct {
	Domain      string
	Port        int
	Host        string
	Ipv4        string
	Advertising BonjourAdvertising
}

func (n *Core) BonjourAdvertise(config BonjourConfig) {
	var err error
	_, err = bonjour.RegisterProxy(
		bonjourSymbols.Instance,
		bonjourSymbols.Service,
		config.Domain,
		config.Port,
		config.Host,
		config.Ipv4,
		[]string{"app=transiant", fmt.Sprintf("%s=%s", bonjourSymbols.NodeAddr, config.Advertising.NodeAddr)},
		nil,
	)
	if err != nil {
		log.Fatalln(err.Error())
	}
}

type BonjourKnownServices map[string]bool

/* @TODO: sanitize strings
 */
func ServiceEntryKV(e *bonjour.ServiceEntry, key string) (string, bool) {
	texts := e.Text
	for i := range texts {
		kv := strings.Split(texts[i], "=")
		if kv[0] == key {
			return kv[1], true
		}
	}
	return "", false
}

func (n *Core) BonjourDiscover() {
	resolver, err := bonjour.NewResolver(nil)
	if err != nil {
		log.Println("Failed to initialize resolver:", err.Error())
		os.Exit(1)
	}
	knownServices := BonjourKnownServices{}
	results := make(chan *bonjour.ServiceEntry)
	go func(results chan *bonjour.ServiceEntry, exitCh chan<- bool) {
		for e := range results {
			value, ok := ServiceEntryKV(e, "node.addr")
			if ok == false {
				log.Printf("node.addr field is missing")
				continue
			}
			_, found := knownServices[value]
			if found == false {
				knownServices[value] = true
				n.channels.get(CHAN_DISCOVER) <- DiscoverMessage{ipv4: value}
			}
		}
	}(results, resolver.Exit)
	for {
		err = resolver.Browse(bonjourSymbols.Service, "local.", results)
		if err != nil {
			log.Println("Failed to browse:", err.Error())
		}
		time.Sleep(60 * time.Second)
	}
}
