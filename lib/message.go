package transiant

import (
	"fmt"
	"log"

	"github.com/google/uuid"
)

type Message interface {
	validateMessage() bool
}

type DebugMessage struct {
	message string
}

func (m DebugMessage) validateMessage() bool {
	return true
}

type DiscoverMessage struct {
	ipv4 string
}

func (m DiscoverMessage) validateMessage() bool {
	return true
}

type CoreMessage struct {
	command   string
	arguments []string
	data      interface{}
}

func (m CoreMessage) validateMessage() bool {
	return true
}

type GossipMessage struct {
	Sender    string // should be public key
	Recipient string // should be public key
	Body      string // should be encrypted by sender
	Signature string // should be signed by sender
	CreatedOn string //
}

func (m GossipMessage) validateMessage() bool {
	return true
}

func (m GossipMessage) init(sender string, recipient string, body string, signature string) bool {
	m.Sender = sender
	m.Recipient = recipient
	m.Body = body
	m.Signature = signature
	return true
}

func makeGossipMessage(sender string, recipient string, body string, signature string) *GossipMessage {
	m := &GossipMessage{}
	m.init(sender, recipient, body, signature)
	return m
}

type ChannelMessage chan Message

type Channels struct {
	count    int
	channels map[Channel]ChannelMessage
}

func (nc *Channels) Init() {
	nc.channels = map[Channel]ChannelMessage{}
}

func (nc *Channels) make(chanIndex Channel) ChannelMessage {
	_, ok := nc.channels[chanIndex]
	if ok == false {
		nc.channels[chanIndex] = make(ChannelMessage)
	}
	return nc.get(chanIndex)
	// return make(ChannelMessage)
}

func (nc *Channels) get(chanIndex Channel) ChannelMessage {
	channel, ok := nc.channels[chanIndex]
	if !ok {
		panic(fmt.Sprintf("ChannelNotFound<%d>", chanIndex))
	}
	return channel
}

func makeChannels() *Channels {
	c := Channels{}
	c.Init()
	return &c
}

func makeUUID() string {
	uuidWithHyphen := uuid.New()
	return uuidWithHyphen.String()
}

func messagesReaderRoutine(ch <-chan Message) {
	id := makeUUID()
	for {
		msg := <-ch
		log.Printf("[%s] Message: %v\n", id, msg)
	}
}
