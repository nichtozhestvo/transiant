package transiant

// import (
// 	"crypto/rand"
// 	"encoding/json"
// 	"fmt"
// 	"io"
// 	"os"
// 	"os/user"
// 	"path"

// 	"golang.org/x/crypto/nacl/secretbox"
// )

// const VAULT_FILENAME = "transiant.vault"

// type VaultData struct {
// 	KeyVerifiers []KeySize
// 	Friends      []KeySize
// 	Keypair      *KeyPair
// }

// func (vd *VaultData) JsonMarshall() ([]byte, error) {
// 	return json.Marshal(vd)
// }

// func (vd *VaultData) JsonUnmarshall(data []byte) error {
// 	return json.Unmarshal(data, vd)
// }

// func makeVaultData() *VaultData {
// 	return &VaultData{
// 		KeyVerifiers: []KeySize{},
// 		Friends:      []KeySize{},
// 	}
// }

// type Vault struct {
// 	sealed []byte
// 	data   *VaultData
// }

// func (v *Vault) Open() (int, error) {
// 	vaultPath, err := findVaultFile()
// 	if err != nil {
// 		return 0, err
// 	}
// 	rh, err := os.Open(vaultPath)
// 	if err != nil {
// 		return 0, err
// 	}
// 	size, err := io.ReadFull(rh, v.sealed)
// 	if err != nil {
// 		return 0, err
// 	}
// 	return size, nil
// }

// func (v *Vault) OpenOrCreate() (int, error) {
// 	size, err := v.Open()
// 	if err == nil {
// 		return size, nil
// 	}
// 	if v.data == nil {
// 		v.data = makeVaultData()
// 		return 1, nil
// 	}
// 	return 0, fmt.Errorf("OpenOrCreateError")
// }

// func (v *Vault) SetKeypair(keypair *KeyPair) error {
// 	err := v.HasData()
// 	if err != nil {
// 		return err
// 	}
// 	v.data.Keypair = keypair
// 	return nil
// }

// func (v *Vault) GetKeypair() (*KeyPair, error) {
// 	err := v.HasData()
// 	if err != nil {
// 		return nil, err
// 	}
// 	if v.data.Keypair == nil {
// 		return nil, fmt.Errorf("KeypairError")
// 	}
// 	return v.data.Keypair, nil
// }

// func (v *Vault) Seal(secretKey KeySize) error {
// 	marshall, err := v.data.JsonMarshall()
// 	if err != nil {
// 		return err
// 	}
// 	var nonce [24]byte
// 	if _, err := io.ReadFull(rand.Reader, nonce[:]); err != nil {
// 		return err
// 	}
// 	enc := secretbox.Seal(nonce[:], marshall, &nonce, secretKey)
// 	v.sealed = enc
// 	v.data = nil
// 	return nil
// }

// func (v *Vault) Unseal(secretKey KeySize) error {
// 	err := v.IsValid()
// 	if err != nil {
// 		return err
// 	}
// 	var decryptNonce [24]byte
// 	copy(decryptNonce[:], v.sealed[:24])
// 	decrypted, ok := secretbox.Open(nil, v.sealed[24:], &decryptNonce, secretKey)
// 	if !ok {
// 		return fmt.Errorf("VaultDecryptionError")
// 	}
// 	var data VaultData
// 	err = data.JsonUnmarshall(decrypted)
// 	if err != nil {
// 		return err
// 	}
// 	v.data = &data
// 	return nil
// }

// func (v *Vault) Write() (int, error) {
// 	if v.sealed == nil || v.data != nil {
// 		return 0, fmt.Errorf("NoSealedOrDataError")
// 	}
// 	choices, err := makeVaultPathChoices()
// 	if err != nil {
// 		return 0, err
// 	}
// 	filepath := path.Join(choices[len(choices)-1], VAULT_FILENAME)
// 	wh, err := os.OpenFile(filepath, os.O_CREATE|os.O_WRONLY, 0660)
// 	if err != nil {
// 		return 0, err
// 	}
// 	count, err := wh.Write(v.sealed)
// 	if err != nil {
// 		return 0, err
// 	}
// 	wh.Close()
// 	return count, nil
// }

// func (v *Vault) HasData() error {
// 	if v.data == nil {
// 		return fmt.Errorf("VaultDataError")
// 	}
// 	return nil
// }

// func (v *Vault) IsValid() error {
// 	if len(v.sealed) == 0 {
// 		return fmt.Errorf("VaultSealedError")
// 	}
// 	if v.data == nil {
// 		return fmt.Errorf("VaultDataError")
// 	}
// 	return nil
// }

// func makeVault() (*Vault, error) {
// 	vault := Vault{}
// 	_, err := vault.OpenOrCreate()
// 	if err != nil {
// 		return nil, err
// 	}
// 	return &vault, nil
// }

// func makeVaultPathChoices() ([]string, error) {
// 	user, err := user.Current()
// 	if err != nil {
// 		return nil, err
// 	}
// 	execPath, err := os.Executable()
// 	if err != nil {
// 		return nil, err
// 	}
// 	execPath = path.Dir(execPath)
// 	possiblePaths := []string{
// 		path.Join(user.HomeDir, ".config"),
// 		path.Join(user.HomeDir, "etc"),
// 		path.Join(user.HomeDir),
// 		path.Join(execPath, "etc"),
// 		path.Join(execPath, ".config"),
// 		path.Join(execPath),
// 	}
// 	return possiblePaths, nil
// }

// func findVaultFile() (string, error) {
// 	possiblePaths, err := makeVaultPathChoices()
// 	if err != nil {
// 		return "", err
// 	}
// 	for _, possible := range possiblePaths {
// 		fullPath := path.Join(possible, VAULT_FILENAME)
// 		if fileExists(fullPath) {
// 			return fullPath, nil
// 		}
// 	}
// 	return "", fmt.Errorf("FileNotFound")
// }
