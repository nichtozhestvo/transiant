package transiant

import (
	"fmt"
)

type VaultItem struct {
	data interface{}
}
type Vault interface {
	Init() error
	Unseal() error
	Seal() error
	Get(string) (*VaultItem, error)
	Set(string, VaultItem) error
}

type VaultManager struct {
	vault Vault
}

func (vm *VaultManager) Init(vault Vault) error {
	if vault == nil {
		return fmt.Errorf("EmptyVaultError")
	}
	vm.vault = vault
	err := vault.Init()
	if err != nil {
		return err
	}
	return nil
}

func (vm *VaultManager) Unseal() error {
	return vm.vault.Unseal()
}

func (vm *VaultManager) Seal() error {
	return vm.vault.Seal()
}

func (vm *VaultManager) Get(key string) (*interface{}, error) {
	item, err := vm.Meta(key)
	if err != nil {
		return nil, err
	}
	return &item.data, nil
}

func (vm *VaultManager) Meta(key string) (*VaultItem, error) {
	item, err := vm.vault.Get(key)
	if err != nil {
		return nil, err
	}
	return item, nil
}

func (vm *VaultManager) Vault() *Vault {
	return &vm.vault
}

func (vm *VaultManager) Set(key string, value interface{}) error {
	return vm.vault.Set(key, VaultItem{data: value})
}

func MakeVaultManager(vault Vault) (*VaultManager, error) {
	vm := VaultManager{}
	err := vm.Init(vault)
	if err != nil {
		return nil, err
	}
	return &vm, nil
}

type VaultMemory struct {
	vault map[string]VaultItem
}

func (vm *VaultMemory) Init() error {
	vm.vault = make(map[string]VaultItem)
	return nil
}
func (vm *VaultMemory) Unseal() error {
	return nil
}

func (vm *VaultMemory) Seal() error {
	return nil
}

func (vm *VaultMemory) Get(key string) (*VaultItem, error) {
	item, ok := vm.vault[key]
	if !ok {
		return nil, fmt.Errorf("KeyError")
	}
	return &item, nil
}

func (vm *VaultMemory) Set(key string, item VaultItem) error {
	vm.vault[key] = item
	return nil
}
