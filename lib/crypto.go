package transiant

import (
	crypto_rand "crypto/rand"
	"time"

	"golang.org/x/crypto/nacl/box"
)

type KeySize *[32]byte

type KeyPair struct {
	Public    KeySize
	Private   KeySize
	CreatedAt time.Time
}

func makeKeyPair() (*KeyPair, error) {
	keyPair := KeyPair{
		CreatedAt: time.Now(),
	}
	public, private, err := box.GenerateKey(crypto_rand.Reader)
	if err != nil {
		return nil, err
	}
	keyPair.Private = private
	keyPair.Public = public
	return &keyPair, nil
}
