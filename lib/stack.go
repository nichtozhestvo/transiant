package transiant

import (
	"fmt"
	"log"
	"strings"
)

type StackType int

const (
	FIFO StackType = iota
	LIFO
)

type Stack interface {
	Init() Stack
	Mode() StackType
	Push(interface{}) error
	Pop() (interface{}, error)
	Size() int
	Clear() error
	SetMode(StackType)
}

type StringStackData []string

type StringStack struct {
	mode  StackType
	stack *StringStackData
}

func (ss StringStack) Mode() StackType {
	return ss.mode
}

func (ss StringStack) Init() Stack {
	ss.mode = FIFO
	ss.stack = &StringStackData{}
	return ss
}

func (ss StringStack) Push(s interface{}) error {
	txt := s.(string)
	*ss.stack = append(*ss.stack, txt)
	return nil
}

func (ss StringStack) SetMode(st StackType) {
	log.Printf("Set mode %d", st)
	ss.mode = st
	log.Printf("SZDZEet mode %d", st)
}

func (ss StringStack) Pop() (interface{}, error) {
	size := ss.Size()
	if size == 0 {
		return "", fmt.Errorf("EmptyStack")
	}
	switch ss.mode {
	case FIFO:
		elm := (*ss.stack)[0]
		*ss.stack = (*ss.stack)[1:]
		return elm, nil
	case LIFO:
		elm := (*ss.stack)[size-1]
		*ss.stack = (*ss.stack)[:size-1]
		return elm, nil
	default:
		panic("StackTypeError")
	}
}

func (ss StringStack) Clear() error {
	stack := *ss.stack
	stack = stack[0:0]
	return nil
}

func (ss StringStack) Size() int {
	stack := *ss.stack
	return len(stack)
}

type StackManager struct {
	stack Stack
}

func (sm StackManager) Init(stack Stack) StackManager {
	sm.stack = stack.Init()
	return sm
}

func (sm *StackManager) Push(e interface{}) error {
	return sm.stack.Push(e)
}

func (sm *StackManager) Pop() (interface{}, error) {
	return sm.stack.Pop()
}

func (sm *StackManager) Size() int {
	return sm.stack.Size()
}

func (sm *StackManager) Clear() error {
	return sm.stack.Clear()
}

func (sm *StackManager) SetMode(mode StackType) {
	sm.stack.SetMode(mode)
}

func (sm *StackManager) Mode() StackType {
	return sm.stack.Mode()
}

func sm_test() {
	pops := func(sm StackManager) {
		for item, err := sm.Pop(); err == nil; item, err = sm.Pop() {
			if err != nil {
				log.Printf("Error %s", err)
				break
			}
			if item == nil {
				log.Fatal("NilItemError")
				break
			}
			txt := item.(string)
			fmt.Printf("%s ", txt)
		}
		fmt.Printf("\n")
	}
	for _, mode := range []StackType{FIFO, LIFO} {
		sm := StackManager{}.Init(&StringStack{})
		for idx, character := range strings.Split("abcdefghijklmnopqrstuvwxyz", "") {
			txt := fmt.Sprintf("%d:%s", idx, character)
			sm.Push(txt)
		}
		sm.SetMode(mode)
		log.Printf("Mode %d", sm.Mode())
		fmt.Printf("%v %+v [Popped] ", sm, sm.stack)
		pops(sm)
	}
}

func Test() {
	sm_test()
}
