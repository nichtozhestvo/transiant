package transiant

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"time"

	"github.com/google/uuid"
	"github.com/hashicorp/memberlist"
)

const VERSION = "0.0.1"

type Core struct {
	Alive      bool
	Uuid       uuid.UUID
	memberList *memberlist.Memberlist
	channels   *Channels
	*KeyPair
	Vault VaultManager
	ipv4  string
}

func (core *Core) Init() *Core {
	if core == nil {
		panic("NilPtr")
	}
	keyPair, err := makeKeyPair()
	if err != nil {
		panic("MakeKeyPairError")
	}
	vm, err := MakeVaultManager(&VaultMemory{})
	if err != nil {
		panic("MakeVaultError")
	}
	core.Alive = true
	core.Uuid = uuid.New()
	core.channels = makeChannels()
	core.KeyPair = keyPair
	core.Vault = *vm
	core.ipv4 = getNodeIpv4()
	core.channels.make(CHAN_CORE)
	core.channels.make(CHAN_DEBUG)
	core.channels.make(CHAN_DISCOVER)
	core.channels.make(CHAN_ASK)
	return core
}

func (n *Core) SendMessage(msg Message, channel Channel) {
	n.channels.get(channel) <- msg
}

func (c *Core) JoinCluster(ip string) error {
	_, err := c.memberList.Join([]string{ip})
	if err != nil {
		return err
	}
	for _, member := range c.memberList.Members() {
		log.Printf("Cluster member: %s %s\n", member.Name, member.Addr)
	}
	return nil
}

func makeMemberList() (*memberlist.Memberlist, error) {
	list, err := memberlist.Create(memberlist.DefaultLocalConfig())
	if err != nil {
		return nil, fmt.Errorf("Failed to create memberlist: %s", err.Error())
	}
	return list, nil
}

func MakeCore() *Core {
	core := Core{}
	core.Init()
	return &core
}

func coreDiscoverRoutine(core *Core, ch <-chan Message) {
	for {
		msg := (<-ch).(DiscoverMessage)
		if core.ipv4 == msg.ipv4 {
			log.Printf("Skipping ourselves (%s)", core.ipv4)
			continue
		}
		err := core.JoinCluster(msg.ipv4)
		if err != nil {
			log.Printf("Joining cluser fail")
			continue
		}
		log.Printf("Cluster joined")
	}
}

func processCoreMessageRoutine(core *Core, ch <-chan Message) {
	for {
		msg := (<-ch).(CoreMessage)
		switch msg.command {
		case "exit":
			log.Printf("Receive exit")
			core.Alive = false
		}
	}
}

func (core *Core) Start() {
	go messagesReaderRoutine(core.channels.get(CHAN_DEBUG))
	go processCoreMessageRoutine(core, core.channels.get(CHAN_CORE))
	go coreDiscoverRoutine(core, core.channels.get(CHAN_DISCOVER))
	log.Printf("[transiant/bonjour] advertising nodeaddr=%s", core.ipv4)
	core.BonjourAdvertise(BonjourConfig{
		Domain:      "",
		Host:        "transiant",
		Ipv4:        "0.0.0.0",
		Port:        9999,
		Advertising: BonjourAdvertising{NodeAddr: core.ipv4},
	})
	log.Printf("[transiant/bonjour] discover nodeaddr=%s", core.ipv4)
	go core.BonjourDiscover()
	wait_terminate := func() {
		signalChan := make(chan os.Signal, 1)
		signal.Notify(signalChan, os.Interrupt, os.Kill)
		s := <-signalChan
		log.Println("Got signal:", s)
		core.Alive = false
		// os.Exit(0)
	}
	go wait_terminate()
	for core.Alive {
		if core.memberList == nil {
			memberList, err := makeMemberList()
			if err != nil {
				log.Printf("Cannot make memberList")
			} else {
				core.memberList = memberList
			}
		}
		time.Sleep(1 * time.Second)
	}
}
