package transiant

import (
	"fmt"

	"github.com/c-bata/go-prompt"
)

type TerminalFn func(ctx *TerminalContext, ts *TerminalStack, scan *TerminalScanPrompt) error

type TerminalStack struct {
	cmd         string
	suggestions []prompt.Suggest
}

func (ts *TerminalStack) AddSuggestion(suggest prompt.Suggest) error {
	ts.suggestions = append(ts.suggestions, suggest)
	return nil
}

func (ts *TerminalStack) makePrompt() string {
	return fmt.Sprintf("%s%s", ts.cmd, PROMPT)
}

func makeTerminalStack(cmd string, suggestions *[]prompt.Suggest) *TerminalStack {
	ts := TerminalStack{
		cmd:         cmd,
		suggestions: []prompt.Suggest{},
	}
	if suggestions != nil {
		for _, suggest := range *suggestions {
			ts.AddSuggestion(suggest)
		}
	}
	return &ts
}
