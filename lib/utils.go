package transiant

import (
	"fmt"
	"net"
	"os"
	"time"
)

type Ipv4 struct {
	iface net.Interface
	addr  net.Addr
	ip    net.IP
}

type Ipv4List []Ipv4

func getIpv4(name *string) (Ipv4List, error) {
	ifaces, err := net.Interfaces()
	if err != nil {
		return nil, err
	}
	iplist := Ipv4List{}
	for _, iface := range ifaces {
		addrs, err := iface.Addrs()
		if err != nil {
			return nil, err
		}
		for _, addr := range addrs {
			var ip net.IP
			switch v := addr.(type) {
			case *net.IPNet:
				ip = v.IP
			case *net.IPAddr:
				ip = v.IP
			}
			ipv4 := ip.To4()
			if ipv4 == nil {
				continue
			}
			if ip.IsLoopback() == true {
				continue
			}
			iplist = append(iplist, Ipv4{
				iface: iface,
				addr:  addr,
				ip:    ip,
			})
		}
	}
	if len(iplist) == 0 {
		return nil, fmt.Errorf("IpNotFoundError")
	}
	return iplist, nil
}

func getNodeIpv4() string {
	iplist, err := getIpv4(nil)
	if err != nil {
		panic(err)
	}
	return iplist[0].ip.String()
}

func timeTrack(core *Core, start time.Time, name string) {
	elapsed := time.Since(start)
	message := fmt.Sprintf("%s took %s", name, elapsed)
	core.SendMessage(DebugMessage{message: message}, CHAN_DEBUG)
}

func elapsedFrom(start time.Time) time.Duration {
	return time.Since(start)
}

type MapStringVal map[string]interface{}

func mapJoin(args ...MapStringVal) MapStringVal {
	root := args[0]
	args = args[1:]
	for _, arg := range args {
		for k, v := range arg {
			root[k] = v
		}
	}
	return root
}

func fileExists(filePath string) bool {
	_, err := os.Stat(filePath)
	if err != nil {
		return false
	}
	return true
}
