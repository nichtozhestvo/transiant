package transiant

import (
	"fmt"
	"log"
	"regexp"
	"strings"

	"github.com/c-bata/go-prompt"
	"github.com/gookit/color"
)

func completer(s []prompt.Suggest) func(d prompt.Document) []prompt.Suggest {
	return func(d prompt.Document) []prompt.Suggest {
		return prompt.FilterHasPrefix(s, d.GetWordBeforeCursor(), true)
	}
}

func askUser(question string, conditions []string, suggestions []prompt.Suggest, tries int) (string, error) {
	test_conconditions := func(response string, condition []string) bool {
		for _, condition := range conditions {
			matched, err := regexp.MatchString(condition, response)
			if err != nil {
				panic(err)
			}
			if matched {
				return true
			}
		}
		return false
	}
	for {
		response := strings.TrimSpace(prompt.Input(question, completer(suggestions)))
		if test_conconditions(response, conditions) {
			return response, nil
		}
		log.Printf("Invalid answer %+v", conditions)
		tries -= 1
		if tries <= 0 {
			break
		}
	}
	return "", fmt.Errorf("AskUserError")
}

func yesNo(answer string) bool {
	answer = strings.ToLower(answer)
	if strings.HasPrefix(answer, "y") {
		return true
	}
	return false
}

func askYesNo(question string, tries int) (bool, error) {
	answer, err := askUser(question,
		[]string{"^y(es)?|n(o)?$"},
		[]prompt.Suggest{
			{Text: "yes"},
			{Text: "no"},
		}, tries)
	if err != nil {
		return false, err
	}
	return yesNo(answer), nil
}

func TerminalStart() {
	sm := makeServiceManager()
	SERVICE_MANAGER = sm
	for _, service := range []Service{MockService{}, CoreService{}, BonjourService{}} {
		sm.Add(&service)
	}
	log.SetFlags(log.Lmsgprefix)
	ctx, err := makeTerminalContext()
	TERMINAL_CTX = ctx
	if err != nil {
		panic(err)
	}
	red := color.FgRed.Render
	stringData := StringStackData{"service start core", "service stop core", "exit"}
	cmdStack := StringStack{stack: &stringData}
	autoCmd := false
	for ctx.alive {
		stack := ctx.currentStack
		lastCmd := GetRootCommand()
		answer := ""
		if !autoCmd {
			answer = prompt.Input(stack.makePrompt(), completer(MakeSuggestions(lastCmd)))
		} else {
			tmp, err := cmdStack.Pop()
			if err != nil {
				log.Fatalf("%s %s\n", red("Error"), err)
				continue
			}
			answer = tmp.(string)
		}
		cmd := scanPrompt(answer)
		se, err := ExecuteCmd(ctx, cmd.Args)
		if err != nil {
			log.Printf("Error %s\n", err)
			continue
		}
		if se.Err() != "" {
			log.Printf("Error %s\n", se.Err())
			continue
		}
		log.Println(se.Out())
	}
}
