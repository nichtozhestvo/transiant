package transiant

import "github.com/c-bata/go-prompt"

type TerminalContext struct {
	alive        bool
	stacks       []*TerminalStack
	currentStack *TerminalStack
	vault        *VaultManager
}

var TERMINAL_CTX *TerminalContext

func (ts *TerminalContext) Push(tsc *TerminalStack) *TerminalStack {
	if tsc == nil {
		panic("PushingNilStack")
	}
	ts.stacks = append(ts.stacks, tsc)
	ts.currentStack = tsc
	return tsc
}

var terminal_stack_root = makeTerminalStack("",
	&[]prompt.Suggest{
		{Text: "exit", Description: "exit program"},
		{Text: "genkey", Description: "generate key"},
		{Text: "keypair", Description: "show key pair"},
		{Text: "unseal", Description: "unseal vault"},
		{Text: "seal", Description: "unseal vault"},
	})

func makeTerminalContext() (*TerminalContext, error) {
	vault, err := MakeVaultManager(&VaultMemory{})
	if err != nil {
		return nil, err
	}
	tc := TerminalContext{
		alive:  true,
		stacks: []*TerminalStack{},
		vault:  vault,
	}
	tc.Push(terminal_stack_root)
	return &tc, nil
}
