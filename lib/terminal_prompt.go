package transiant

import (
	"strings"
	"text/scanner"
)

const (
	PROMPT = "# "
)

type TerminalScanPrompt struct {
	Cmd  string
	Args []string
}

func makeScanPromptData(cmd string, args []string) TerminalScanPrompt {
	return TerminalScanPrompt{
		Cmd:  cmd,
		Args: args,
	}
}

func scanPrompt(src string) TerminalScanPrompt {
	data := makeScanPromptData("", []string{})
	var s scanner.Scanner
	s.Init(strings.NewReader(src))
	s.Filename = "comments"
	s.Mode ^= scanner.SkipComments // don't skip comments
	for tok := s.Scan(); tok != scanner.EOF; tok = s.Scan() {
		txt := s.TokenText()
		if data.Cmd == "" {
			data.Cmd = txt
		}
		data.Args = append(data.Args, txt)
	}
	return data
}
