package transiant

type Channel int

const (
	CHAN_DEBUG Channel = iota
	CHAN_DISCOVER
	CHAN_ASK
	CHAN_CORE
)
