ISDEV=1
VERSION=0.0.1
NAME=showi/nichtozhestvo-transiant
TAG=$(NAME):$(VERSION)
BASE_IMG=$(shell docker images | grep "$(NAME).*$(VERSION)-base")
BUILD_FLAGS=-ldflags "-s -w"
SWARM_NUM=3
ifndef NETWORK
	NETWORK=transiant
endif
DOCKER_OPTS=--network $(NETWORK)
ifdef ISDEV
	DOCKER_OPTS+=-v $(shell pwd):/build
	BUILD_FLAGS=
endif
$(info $(DOCKER_OPTS))

.PHONY: test
test:
	go test

bin/transiant:
	mkdir -p bin
	go build $(BUILD_FLAGS) -o bin/transiant main.go

.PHONY: clean_build
clean_build:
	rm -f bin/transiant

.PHONY: build
build: clean_build bin/transiant

.PHONY: run
run: build
	./bin/transiant

.PHONY: docker-build-base
docker-build-base:
ifeq  "$(BASE_IMG)" ""
	docker build -f Dockerfile.base . --tag $(TAG)-base
else
	$(info "BASE ALREADY BUILD $(TAG)-base")
endif

.PHONY: docker-build
docker-build: docker-build-base 
	docker build . --tag $(TAG)

.PHONY: docker-run
docker-run: docker-build docker-only-run

.PHONY: docker-only-run
docker-only-run: 
	docker run $(DOCKER_OPTS) -it $(TAG) $(CMD)

.PHONY: swarm
swarm:
	docker-compose build && docker-compose up --scale node=$(SWARM_NUM)

.PHONY: clean
clean:
	rm -rf bin/
	docker image rm -f $(TAG)
	docker image rm -f $(TAG)-base