module gitlab.com/nichtozhestvo/transiant

go 1.15

require (
	github.com/c-bata/go-prompt v0.2.5
	github.com/gizak/termui/v3 v3.1.0
	github.com/google/uuid v1.2.0
	github.com/gookit/color v1.3.7
	github.com/hashicorp/memberlist v0.2.2
	github.com/jamesruan/sodium v0.0.0-20181216154042-9620b83ffeae
	github.com/oleksandr/bonjour v0.0.0-20160508152359-5dcf00d8b228
	github.com/spf13/cobra v1.1.1
	github.com/urfave/cli/v2 v2.3.0
	golang.org/x/crypto v0.0.0-20190923035154-9ee001bba392
)
