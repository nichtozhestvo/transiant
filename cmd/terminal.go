package cmd

import (
	"github.com/spf13/cobra"
	transiant "gitlab.com/nichtozhestvo/transiant/lib"
)

func init() {
	rootCmd.AddCommand(terminalCmd)
}

var terminalCmd = &cobra.Command{
	Use:   "terminal",
	Short: "start transiant terminal",
	Long:  `see --help`,
	Run: func(cmd *cobra.Command, args []string) {
		transiant.Test()
		transiant.TerminalStart()
	},
}
