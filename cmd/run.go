package cmd

import (
	"github.com/spf13/cobra"
	transiant "gitlab.com/nichtozhestvo/transiant/lib"
)

func init() {
	rootCmd.AddCommand(runCmd)
}

var runCmd = &cobra.Command{
	Use:   "run",
	Short: "Execute transiant",
	Long:  `see --help`,
	Run: func(cmd *cobra.Command, args []string) {
		core := transiant.MakeCore()
		core.Start()
	},
}
