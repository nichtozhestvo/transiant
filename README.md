# Transient

Playing with [golang](https://golang.org/) and [libs](./go.mod).

## build & run

see [Makefile](./Makefile).

```sh
make build
make run
```

Binary in `./bin`.

## docker

see [Dockerfile](./Dockerfile).

```sh
make run
```

## swarm

see [docker-compose](./docker-compose.yml)

```sh
make swarm
```
