package main

import (
	"testing"

	transiant "gitlab.com/nichtozhestvo/transiant/lib"
)

func TestVersion(t *testing.T) {
	want := "0.0.1"
	if got := transiant.VERSION; got != want {
		t.Errorf("VERSION = %q, want %q", got, want)
	}
}

func TestVaultManager(t *testing.T) {
	key := "k/Foo"
	want := "v/Bar"
	mem := transiant.VaultMemory{}
	vm, err := transiant.MakeVaultManager(&mem)
	if err != nil {
		t.Errorf("MakeVaultManagerError")
	}
	err = vm.Set(key, want)
	if err != nil {
		t.Errorf("SetError")
	}
	vi, err := vm.Get(key)
	if err != nil {
		t.Errorf("GetError")
	}
	if got := (*vi).(string); got != want {
		t.Errorf("VERSION = %q, want %q", got, want)
	}
}
