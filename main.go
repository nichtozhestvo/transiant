package main

import (
	"gitlab.com/nichtozhestvo/transiant/cmd"
)

func main() {
	cmd.Execute()
}
